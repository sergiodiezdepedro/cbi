function panel(id) {
    if (document.getElementById) {
        var x = document.getElementById(id);
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        } 
    }
}

let acordeon = document.getElementsByClassName("main__sidebar__opciones__trigger");
let i;

for (i = 0; i < acordeon.length; i++) {
  acordeon[i].addEventListener("click", function() {
    this.classList.toggle("abrir");
    let panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}